package com.performance;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static final int TASK_COUNT = 24;
    public static final ExecutorService EXECUTOR_SERVICE = Executors.newWorkStealingPool();
    public static final int DIFFICULTY = 300000000;

    public static void main(String[] args) {
        System.out.println("processors: " + Runtime.getRuntime().availableProcessors());
        Utils.init();
        System.out.println(new Calculations().longTask());
        System.out.println("processors: " + Runtime.getRuntime().availableProcessors());
        System.out.println("task count: " + TASK_COUNT);
        long start = System.nanoTime();
        List<Callable<Integer>> tasks = new ArrayList<>();
        for (int i = 0; i < TASK_COUNT; i++) {
            Calculations calculations = new Calculations();
            tasks.add(calculations::longTask);
        }
        try {
            EXECUTOR_SERVICE.invokeAll(tasks);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }
        long result = System.nanoTime() - start;
        System.out.println(Utils.formatTime(result));
        EXECUTOR_SERVICE.shutdown();
        try {
            Thread.sleep(70000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("processors: " + Runtime.getRuntime().availableProcessors());
        try {
            Thread.sleep(1000 * 60 * 5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

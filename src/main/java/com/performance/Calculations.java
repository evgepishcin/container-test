package com.performance;

import static com.performance.Main.DIFFICULTY;

public class Calculations {

    public Integer longTask() {
        System.out.println("job start in thread " + Thread.currentThread().getName());
        long start = System.nanoTime();
        int sum = 0;
        for (int i = 0; i < DIFFICULTY; i++) {
            int val = Utils.getRandomVal(i);
            int min = Math.min(sum, val);
            sum = (Math.abs(sum - val) / 2) + min;
        }
        long result = System.nanoTime() - start;
        System.out.println("job end in thread " + Thread.currentThread().getName() + ", time: " + Utils.formatTime(result));
        return sum;
    }


}

package com.performance;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class Utils {
    private static final List<Integer> SEQUENCE = new ArrayList<>();
    public static void init() {
        for (int i = 0; i < 1000; i++) {
            SEQUENCE.add(new Random().nextInt(1000));
        }
    }

    public static int getRandomVal(int index) {
        return SEQUENCE.get(index % SEQUENCE.size());
    }

    public static String formatTime(long time) {
        long seconds = time / 1000000000;
        long milliseconds = (time - (seconds * 1000000000)) / 1000000;
        return  ("seconds: " + seconds + ", milliseconds: " + milliseconds);
    }
}
